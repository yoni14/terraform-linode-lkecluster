variable "label" {
  type = string
}

variable "k8s_version" {
  type = string
}

variable "region" {
  type = string
}

variable "tags" {
  type = list(string)
}

variable "pools" {
  type = list(object({
    type  = string
    count = number
  }))
  default = [{
    type  = "g6-standard-1"
    count = 1
  }]
}
